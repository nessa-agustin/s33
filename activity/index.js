// console.log('hello')

const url = "https://jsonplaceholder.typicode.com/todos";

// GET ALL
fetch(url)
    .then((response) => response.json())
    .then((json) => {

        let titles = json.map(t => t.title);

        console.log(titles)        

    })


// GET ONE
fetch(url + '/1')
.then((response) => response.json())
.then((json) => {

    json.status = "Completed";

    console.log(json)        

})

// POST
fetch(url, {
    method: "POST",
    headers: {
        "Content-Type":"application/json"
    },
    body: JSON.stringify({
        userId: 1,
        title: "Run errands",
        completed: false
    })
})
.then((res) => res.json())
.then((json) => console.log(json))

// PUT
fetch(url + '/3', {
    method: "PUT",
    headers: {
        "Content-Type":"application/json"
    },
    body: JSON.stringify({
        userId: 1,
        title: "Run errands",
        description: "Go to grocery, take out trash",
        dateCompleted: '2022-08-24',
        completed: true
    })
})
.then((res) => res.json())
.then((json) => console.log(json))

// PATCH
fetch(url + '/3', {
    method: "PATCH",
    headers: {
        "Content-Type":"application/json"
    },
    body: JSON.stringify({
        dateStatusChanged: '2022-08-26',
        status: "Complete"
    })
})
.then((res) => res.json())
.then((json) => console.log(json))

// DELETE
fetch(url + '/3', {method: "DELETE"})